package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        if (statement == null ||
                statement.isEmpty() ||
                statement.startsWith("-(") ||
                statement.contains("++") ||
                statement.contains("--") ||
                statement.contains("**") ||
                statement.contains("//") ||
                statement.contains("..") ||
                statement.contains("+-") ||
                statement.contains("-+") ||
                statement.contains("+*") ||
                statement.contains("*+") ||
                statement.contains("+/") ||
                statement.contains("/+") ||
                statement.contains("+.") ||
                statement.contains(".-") ||
                statement.contains("-*") ||
                statement.contains("*-") ||
                statement.contains("-/") ||
                statement.contains("/-") ||
                statement.contains("-.") ||
                statement.contains("*/") ||
                statement.contains("/*") ||
                statement.contains("*.") ||
                statement.contains(".*") ||
                statement.contains("/.") ||
                statement.contains("./") ||
                statement.charAt(0) == '+' ||
                statement.charAt(0) == '*' ||
                statement.charAt(0) == '/' ||
                statement.contains(",")) return null;

        if (!checkBrackets(statement)) return null;

        List<String> stringList = new ArrayList<>();
        String temp = "";
        for (int i = 0; i < statement.length(); i++) {
            if (statement.substring(i, i + 1).equals("+") ||
                    statement.substring(i, i + 1).equals("-") ||
                    statement.substring(i, i + 1).equals("*") ||
                    statement.substring(i, i + 1).equals("/") ||
                    statement.substring(i, i + 1).equals("(") ||
                    statement.substring(i, i + 1).equals(")")
            ) {
                if (!temp.equals("")) stringList.add(temp);
                temp = statement.substring(i, i + 1);
                stringList.add(temp);
                temp = "";
            } else {
                temp += statement.substring(i, i + 1);
                if (i == statement.length() - 1) {
                    stringList.add(temp);
                }
            }
        }

        String res = calc(stringList);

        if (res != null) {
            try {
                double num;
                num = Double.parseDouble(res);
                if (num % 1.0 != 0)
                    res = String.format("%s", num);
                else
                    res = String.format("%.0f", num);

            } catch (NumberFormatException ignored) {
            }
        }
        return res;
    }

    private String calc(List<String> stringList) {
        String res;

        if (stringList.contains("(") || stringList.contains(")")) {
            res = calc(stringList.subList(stringList.lastIndexOf("(") + 1, stringList.indexOf(")")));
            if (res == null) return null;
            int firstNum = stringList.lastIndexOf("(");
            stringList.add(firstNum, res);
            int num = stringList.indexOf(")") - stringList.lastIndexOf("(") + 1;
            for (int i = 0; i < num; i++) {
                stringList.remove(firstNum + 1);
            }
        }

        if (stringList.size() == 3) {
            switch (stringList.get(1)) {
                case "+": {
                    double num1;
                    try {
                        num1 = Double.parseDouble(stringList.get(0));
                    } catch (NumberFormatException e) {
                        return null;
                    }
                    double num2;
                    try {
                        num2 = Double.parseDouble(stringList.get(2));
                    } catch (NumberFormatException e) {
                        return null;
                    }
                    num1 = num1 + num2;
                    return Double.toString(num1);
                }
                case "-": {
                    double num1;
                    try {
                        num1 = Double.parseDouble(stringList.get(0));
                    } catch (NumberFormatException e) {
                        return null;
                    }
                    double num2;
                    try {
                        num2 = Double.parseDouble(stringList.get(2));
                    } catch (NumberFormatException e) {
                        return null;
                    }
                    num1 = num1 - num2;
                    return Double.toString(num1);
                }
                case "*": {
                    double num1;
                    try {
                        num1 = Double.parseDouble(stringList.get(0));
                    } catch (NumberFormatException e) {
                        return null;
                    }
                    double num2;
                    try {
                        num2 = Double.parseDouble(stringList.get(2));
                    } catch (NumberFormatException e) {
                        return null;
                    }
                    num1 = num1 * num2;
                    return Double.toString(num1);
                }
                case "/": {
                    double num1;
                    try {
                        num1 = Double.parseDouble(stringList.get(0));
                    } catch (NumberFormatException e) {
                        return null;
                    }
                    double num2;
                    try {
                        num2 = Double.parseDouble(stringList.get(2));
                    } catch (NumberFormatException e) {
                        return null;
                    }
                    if (num2 == 0) {
                        return null;
                    } else {
                        num1 = num1 / num2;
                        return Double.toString(num1);
                    }
                }
            }
        }

        if (stringList.contains("/") || stringList.contains("*")) {
            int firstNum;
            if (stringList.contains("/")) {
                firstNum = stringList.indexOf("/") - 1;
                res = calc(stringList.subList(stringList.indexOf("/") - 1, stringList.indexOf("/") + 2));
            } else {
                firstNum = stringList.indexOf("*") - 1;
                res = calc(stringList.subList(stringList.indexOf("*") - 1, stringList.indexOf("*") + 2));
            }
            if (res == null) return null;
            stringList.add(firstNum, res);
            for (int i = 0; i < 3; i++) {
                stringList.remove(firstNum + 1);
            }
        } else if (stringList.contains("-")) {
            int firstNum;
            firstNum = stringList.indexOf("-") - 1;
            res = calc(stringList.subList(stringList.indexOf("-") - 1, stringList.indexOf("-") + 2));
            if (res == null) return null;
            stringList.add(firstNum, res);
            for (int i = 0; i < 3; i++) {
                stringList.remove(firstNum + 1);
            }
        } else if (stringList.contains("+")) {
            int firstNum;
            firstNum = stringList.indexOf("+") - 1;
            res = calc(stringList.subList(stringList.indexOf("+") - 1, stringList.indexOf("+") + 2));
            if (res == null) return null;
            stringList.add(firstNum, res);
            for (int i = 0; i < 3; i++) {
                stringList.remove(firstNum + 1);
            }
        }
        return calc(stringList);
    }

    private boolean checkBrackets(String statement) {
        if (statement.contains("(")) {
            if (statement.lastIndexOf(")") == -1) {
                return false;
            } else {
                if (statement.lastIndexOf(")") < statement.indexOf("(")) {
                    return false;
                } else {
                    statement = deleteChar(statement, statement.lastIndexOf(")"));
                    statement = deleteChar(statement, statement.indexOf("("));
                    return checkBrackets(statement);
                }
            }
        } else return !statement.contains(")");
    }

    private String deleteChar(String str, int num) {
        if (num == 0) return str.substring(num + 1);
        return str.substring(0, num) + str.substring(num + 1);
    }
}
