package com.tsystems.javaschool.tasks.pyramid;

import java.sql.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        int i;
        int j;
        boolean isBuilded = false;
        int width = 0;
        int height = 0;

        if(inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        for(i=0;i<Integer.MAX_VALUE;i++){
            if((i*(i+1)/2) == inputNumbers.size()) {
                isBuilded=true;
                height = i;
                width = height*2-1;
                break;
            }
        }

        if(!isBuilded) throw new CannotBuildPyramidException();

        Collections.sort(inputNumbers);

        int[][] matrix;
        try {
            matrix = new int[height][width];
        }catch (OutOfMemoryError e){
            throw new CannotBuildPyramidException();
        }

        int k = 0;
        for(i=0;i<height;i++){
            for(j=0;j<width;j++){
                if(height%2!=0){
                    if ((((j>=height-i-1) && (width-height+i+1>j) ) && (j%2==0) && (i%2==0)) || (((j>=height-i-1) && (width-height+i+1>j) ) && (j%2!=0) && (i%2!=0))) {
                        matrix[i][j]=inputNumbers.get(k);
                        k++;
                    } else {
                        matrix[i][j]=0;
                    }
                } else {
                    if ((((j>=height-i-1) && (width-height+i+1>j) ) && (j%2==0) && (i%2!=0)) || (((j>=height-i-1) && (width-height+i+1>j) ) && (j%2!=0) && (i%2==0))) {
                        matrix[i][j]=inputNumbers.get(k);
                        k++;
                    } else {
                        matrix[i][j]=0;
                    }
                }

            }
        }
        return matrix;
    }
}
